#! /usr/bin/python3

import pexpect
import pexpect.replwrap
import sys

c = pexpect.replwrap.REPLWrapper(
        pexpect.spawnu(
            command=sys.argv[1],
            echo=False,
            logfile=sys.stdout
        ),
        orig_prompt=sys.argv[2],
        prompt_change=None
    )
for line in sys.argv[3:]:
    c.run_command(line)
print()
